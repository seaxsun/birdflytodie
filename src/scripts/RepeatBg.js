let widBird
export default class GameRoot extends Laya.Script{
    constructor(){
        super();
    }
    onAwake(){
        widBird = this.owner.width;
    }
    onUpdate(){
        if (this.owner.x <= -widBird){
            this.owner.x += widBird*2;
        }
    }
}